import os
from langchain.chat_models import ChatOpenAI
from langchain.embeddings import OpenAIEmbeddings
from langchain.vectorstores import Chroma
from langchain.chains import ConversationalRetrievalChain
from langchain.memory import ConversationBufferMemory
from langchain.llms import OpenAI
from langchain.memory import MongoDBChatMessageHistory
from langchain.document_loaders.recursive_url_loader import RecursiveUrlLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter
from bs4 import BeautifulSoup as Soup
from langchain.utils.html import (PREFIXES_TO_IGNORE_REGEX,
                                  SUFFIXES_TO_IGNORE_REGEX)
import logging
import sys
from config import *

logging.basicConfig(stream=sys.stdout, level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

global conversation
global vectordb
conversation = None
vectordb = None

def init_index():
    if not INIT_INDEX:
        logging.info("continue without initializing index")
        return

    logging.info("initializing index from `%s`", TARGET_URL)

    # scrape data from web
    documents = RecursiveUrlLoader(
        TARGET_URL,
        max_depth=4,
        extractor=lambda x: Soup(x, "html.parser").text,
        prevent_outside=True,
        use_async=True,
        timeout=600,
        check_response_status=True,
        # drop trailing / to avoid duplicate pages.
        link_regex=(
            f"href=[\"']{PREFIXES_TO_IGNORE_REGEX}((?:{SUFFIXES_TO_IGNORE_REGEX}.)*?)"
            r"(?:[\#'\"]|\/[\#'\"])"
        ),
    ).load()

    logging.info("index created with `%d` documents", len(documents))

    # split text
    text_splitter = RecursiveCharacterTextSplitter(chunk_size=4000, chunk_overlap=200)
    spllited_documents = text_splitter.split_documents(documents)

    # create embedding and persist on vector db
    embeddings = OpenAIEmbeddings()
    vectordb = Chroma.from_documents(
        documents=spllited_documents,
        embedding=embeddings,
        persist_directory=INDEX_PERSIST_DIRECTORY
    )
    vectordb.persist()

def init_conversation():
    global vectordb
    global conversation

    # load index
    embeddings = OpenAIEmbeddings()
    vectordb = Chroma(persist_directory=INDEX_PERSIST_DIRECTORY,embedding_function=embeddings)

    # create conversation with gpt-4 llm
    # max_tokens - define maximum output token limit from gpt
    # max_tokens_limit - defines maximum input token limit to gpt
    conversation = ConversationalRetrievalChain.from_llm(
        ChatOpenAI(temperature=0.7, model_name="gpt-4", max_tokens=1024),
        vectordb.as_retriever(),
        max_tokens_limit=8192,
        return_source_documents=True,
        verbose=True,
    )

    return conversation

def chat(question, user_id):
    global conversation

    # constrct chat history with mongodb
    # session_id is the user_id which comes through http request
    # mongo connection string (e.g mongodb://localhost:27017/)
    connection_string = f"mongodb://{MONGO_HOST}:{MONGO_PORT}/"
    history = MongoDBChatMessageHistory(
        connection_string=connection_string, session_id=user_id
    )

    # gpt conversation
    result = conversation({"question": question, "chat_history": history.messages})
    answer = result['answer']

    logging.info("got response from llm - %s", answer)

    # save history
    history.add_user_message(question)
    history.add_ai_message(answer)

    return answer
